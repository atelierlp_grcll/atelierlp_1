﻿<?php

// --- 
// ----------- PAGE D'ACCUEIL 
// --- 
$app->get('/', function () use ($app) {

    $app->redirect($app->urlFor('home'));

})->name('/');

$app->get('/home', function () use ($app) {
	
	//Récupération des documents
	$docs = Documents::limit(8)->orderBy('id', 'DESC')->get();
	

	// verification connexion
	$session="";
	if(isset($_SESSION["adherent"]) ){
		$session =  $_SESSION['adherent'];
	}
 	$app->render('accueil.twig', array(
 		"session" => $session,
    	'docs' => $docs
    	));

})->name('home');


// --- 
// ---------- PAGE DE RECHERCHE
// --- 
$app->get('/recherche', function () use ($app) {
	$types = Type::all();
	$genres = Genre::all();

	// verification connexion
	$session="";
	if(isset($_SESSION["adherent"])){
		$session =  $_SESSION['adherent'];
	}
    $app->render('recherche.twig', array(
    	'session' => $session,
    	'types'  => $types,
    	'genres' => $genres,
    	'idPage' => 1
    ));
});


// --- 
// PAGE DE RESULTAT
// --- 
$app->post('/rechercheResult', function () use ($app) {
	$types = Type::all();
	$genres = Genre::all();	


	// mot cle 
	$motCle = filter_var($app->request->post('rech-libre'),FILTER_SANITIZE_STRING);
	$query="(descriptif LIKE '%$motCle%' OR titre LIKE '%$motCle%')";

	// type 
	$type = filter_var($app->request->post('types'),FILTER_SANITIZE_NUMBER_INT);
	
	if ( $type>0) {
		$query .= " AND idType = ".$type;
	}
	$genre =  filter_var($app->request->post('genres'),FILTER_SANITIZE_NUMBER_INT);
	if ( $genre>0) {
		$query .= " AND idGenre = ".$genre;
	}


	$resRecherche = Documents::whereRaw($query)->get();


	// $resRecherche = Documents::whereRaw($query)->get();
	// verification connexion
	$session="";
	if(isset($_SESSION["adherent"])){
		$session =  $_SESSION['adherent'];
	}
    $app->render('recherche.twig', array(
    	"session" => $session,
    	'types'  => $types,
    	'genres' => $genres,
    	'resultats' => $resRecherche,
    	'typeDoc' => $type,
    	'genreDoc' => $genre
    ));
});


// --- 
// ------- PAGE D'UN DOCUMENT
// --- 
$app->get('/document:id', function ($id) use ($app){
	$id = filter_var($id,FILTER_SANITIZE_NUMBER_INT);

	// vérificaiton du get id du document
	if(!empty($id)){
		$document = Documents::with('Type','Etat')->find($id);
	} else {
		$app->redirect($app->urlFor('notife', array(
			'msg' => "Le document n'existe pas"
		)));
	}

	// Si le document est emprunté alors on récupère l'instance de l'emprunt
	if($document->idEtat === 2 ){
		$emprunt = Emprunt::where('idDoc', '=', $id)->first();
	}  else{
		$emprunt = "";
	}



	// on vérifie si le document est toujours en cours de réservation
	$dateJour = strtotime(date("d-m-Y"));
	$dateReser = strtotime($document->reservation);
	if($dateJour > $dateReser){ // si ce n'est plus reservé  alors l'id doit etre à 3
		if($document->idEtat === 3 ){  // on remet l'id etat a disponible si ce n'est pas déja fait
			$document->idEtat = 1;
			$document->save();
		}
		$reserver = 1;
	} else {
		$reserver = 0;
	}

	// verification connexion
	$session="";
	if(isset($_SESSION["adherent"])){
		$session =  $_SESSION['adherent'];
	}

	$app->render('document.twig',array(
		'session' => $session,
		'emprunt' => $emprunt,
		'documents' => $document,
		'reserver' => $reserver
	));

})->name('document');

// --- 
// ---------- RESERVATION 
// --- 
$app->post('/reserver:id', function($id) use ($app){
	$idsession="";
	if(isset($_SESSION["adherent"])){
		$idsession =  $_SESSION['adherent']['adheid'];
		
		$id = filter_var( $id, FILTER_SANITIZE_NUMBER_INT);
		if(empty($id)){
			$app->redirect($app->urlFor('notifd', array(
				'msg' => "Erreur de page"
			)));
		}
		$document = Documents::find($id);

		if(empty($document)){
			$app->redirect($app->urlFor('notife', array(
				'msg' => "Désolé, ce document n'existe pas"
			)));
		}

		if($document->idEtat == 1){	// Si le document est disponible
			$document->reservation =  date('d-m-Y', strtotime('+15 days'));
		}else if($document->idEtat == 2){ // si le document est emprunté
			$emprunt = Emprunt::where("idDoc", "=", $document->id)->first();
			$document->reservation =  date('d-m-Y', strtotime($emprunt->dateRetour.'+15 days'));
		}else{ // sinon le document est indisponible car déjà réservé ou autres
			$app->redirect($app->urlFor('notife', array(
				'msg' => "Réservation impossible"
			)));
		}

		$document->idAdherent = $idsession;
		$document->idEtat = 3;
		$document->save();	

		$app->redirect($app->urlFor("document", array(
			'id' => $id
		)));

	} else {
		$app->redirect($app->urlFor('connexion', array(
			'id' => $id
		)));
	} 
});




// --- 
// -------------------- ESPACE ADHERENT 
// --- 

// ---------- Page de connexion  
$app->get('/connexion', function () use ($app){

	// verification connexion
	$session="";
	if(isset($_SESSION["adherent"])){
		$session =  $_SESSION['adherent'];
	}

	$app->render('connexion.twig', array(
		'session' => $session
		));
})->name('connexion');


// ----- notification de la connexion
$app->post('/notifConnex', function () use ($app){

	$id = filter_var($app->request->post('idAdherent'),FILTER_SANITIZE_NUMBER_INT); 
	$mdpAdh = filter_var($app->request->post('mdpAdh'),FILTER_SANITIZE_STRING);

	$adherent = Adherents::find($id);
	
	if(empty($adherent->id)){
		$app->redirect($app->urlFor('notife', array(
			'msg' => "L'adhérent n'existe pas"
		)));
	} 

	if($adherent->password === crypt($mdpAdh,'$5$rounds=5000$utiliseselpourmedianet$')){
		
		$_SESSION['adherent']= array(
				'nom' => $adherent->nom,
				'prenom' => $adherent->prenom,
				'email' => $adherent->email,
				'adheid' => $adherent->id,
				'adhe_ip' => $_SERVER["REMOTE_ADDR"]
		);
	} else {
		$app->redirect($app->urlFor('notife', array(
			'msg' => "Mot de passe incorrect"
		)));
	}
	
	$app->redirect($app->urlFor('home'));
});


// ----- DECONNEXION
$app->get('/notifDeconnex', function () use ($app){
	session_destroy();
	$app->redirect($app->urlFor('home'));
});


// --- 
// -------- GESTION EMPRUNT 
// --- 
$app->get("/gestionEmprunt:id", function ($id) use ($app){

	$id = filter_var($id,FILTER_SANITIZE_NUMBER_INT);
	if(!empty($id)){
		$emprunts = Emprunt::where("idAdherent","=",$id)->with("document")->get();
	} else {
		$app->redirect($app->urlFor('notifd', array(
			'msg' => "Erreur de page"
		)));
	}

	$session="";
	if(isset($_SESSION["adherent"])){
		$session =  $_SESSION['adherent'];
	}

	$app->render("gestionEmprunt.twig",array(
		"session" => $session,
		"id" => $id,
		"emprunts" => $emprunts
		));

});



// --- 
// ---------------------- LES NOTIFICATIONS 
// --- 

$app->get('/danger:msg', function ($msg ='') use ($app) {

	$app->render('notification.twig', array(
		'msg' => $msg,
		'coulAlert' => 'alertDanger',
		'titreAlert' => 'Attention !'
	));
})->name('notifd');

$app->get('/success:msg', function ($msg ='') use ($app) {

	$app->render('notification.twig', array(
		'msg' => $msg,
		'coulAlert' =>'alertSuccess',
		'titreAlert' => 'Succès'
	));
})->name('notifs');


$app->get('/info:msg', function ($msg ='') use ($app) {

	$app->render('notification.twig', array(
		'msg' => $msg,
		'coulAlert' => 'alertInfo',
		'titreAlert' =>'Information'
	));
})->name('notifi');

$app->get('/erreur:msg', function ($msg ='') use ($app) {
	
	$app->render('notification.twig', array(
		'msg' => $msg,
		'coulAlert' =>'alertAdvert',
		'titreAlert' =>'Erreur'
	));
})->name('notife');
 ?>
