<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Genre extends Eloquent
{
  protected $table = 'genre';

  protected $hidden = array('password');


  /**
  * Récupère les documents du genre
  *
  */
  public function documents()
  {
    return $this->hasMany('Documents','idGenre');
  }
}
 ?>
