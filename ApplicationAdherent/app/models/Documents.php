<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Documents extends Eloquent
{
  protected $table = 'documents';

  protected $hidden = array('password');

  public function etat()
  {
    return $this->belongsTo('Etat','idEtat');
  }


  /**
  * Récupère le genre du document
  *
  */ 
  public function genre()
  {
    return $this->belongsTo('Genre','idGenre');
  }

  /**
  * Récupère le type du document
  *
  */ 
  public function type()
  {
    return $this->belongsTo('Type','idType');
  }
}
 ?>
