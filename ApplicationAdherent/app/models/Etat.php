<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Etat extends Eloquent
{
  protected $table = 'etat';

  protected $hidden = array('password');

  /**
  * Récupère les documents de l'etat
  */
  public function documents()
  {
    return $this->hasMany('Documents','idEtat');
  }
}
 ?>
