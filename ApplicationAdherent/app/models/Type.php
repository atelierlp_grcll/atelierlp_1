<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Type extends Eloquent
{
  protected $table = 'type';

  protected $hidden = array('password');


  /**
  * Récupère les documents du type
  *
  */
  public function documents()
  {
    return $this->hasMany('Documents','idType');
  }
}
 ?>
