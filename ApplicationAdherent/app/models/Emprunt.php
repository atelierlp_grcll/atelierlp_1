<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Emprunt extends Eloquent
{
  protected $table = 'emprunt';

  protected $hidden = array('password');


  /**
  * Récupère le document de l'emprunts
  *
  */ 
  public function document()
  {
    return $this->belongsTo('Documents','idDoc');
  }

  /**
  * Récupère l'adhérent de l'emprunt
  *
  */ 
  public function adherent()
  {
    return $this->belongsTo('Adherents','idAdherent');
  }
}
 ?>
