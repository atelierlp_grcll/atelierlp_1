<?php
require '../vendor/autoload.php';

session_cache_limiter(false);
session_start();

$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig(),
    'templates.path' => '../app/templates'
));

require '../app/routes.php';

$app->run();

?>
