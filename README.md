# README #

## Auteur 

Kévin GUILLAUME
Nathalie RIO
Nicolas CHAMPION
Kévin LEGOUGNE
Gaetan LOGEARD

## Explication git (pour les membres du groupe)

Chaque membre du groupe **crée une branche portant leur nom**.
Chacun doit s'assurer qu'il **travaille sur sa branche**. 
Lorsque vous voulez ajouter votre travail sur git :

Faites un **git status** afin de visualiser les fichiers modifiés, supprimés ou ajoutés. 

Si vous avez supprimé des fichiers faites un **git rm <nom_du_fichier>** 
sinon faites un **git add .** (afin d'ajouter les modifications que vous avez effectué dans l'index). 

Verifié que les fichiers sont dans l'index avec un **git status**,

Puis faites  **git commit -m "Description de votre commit"**.

Enfin faites : **git push -u origin <nom_de_votre_branche>**.

Signalez à la personne gérant la branche master du push.

Afin de récuperer les modifications sur votre machine faites un **git pull origin master**.

### Pour la branche master 

Afin de réunir les modifications, faites :

1. **git pull origin master** pour récuperer les modifications en local
2. **git merge origin <nom_de_la_branche_a_merger>** ou **git pull origin <nom_de_la_branche_a_merger>** pour ajouter les modifications de la branche spécifique à la branche master
3. **git push -u origin master** pour mettre en ligne les modifications sur la branche master.

## Configuration des sites

*Pour la première* : faites un **composer install** dans un terminal a l'emplacement du composer.json afin d'avoir le dossier *vendor*. Si le composer.json change, faites un **composer update**.  

*Interface* : Utilisation de compass. Un fichier .bat permet de modifier les fichiers sass (com_pass.bat). 
N'hésitez pas à utilisé le cours sur twig pour comprendre le fonctionnement des blocks.

*Serveur* : Le script de base de données est dans la racine du projet "script/base".
Créez un fichier **"app.config.ini"** qui contient le tableau pour la configuration de la base de donnée. Ce fichier est a mettre dans le dossier **app** de chaque application.