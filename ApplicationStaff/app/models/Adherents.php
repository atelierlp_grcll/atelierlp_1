<?php


use Illuminate\Database\Eloquent\Model as Eloquent;

class Adherents extends Eloquent{


  protected $table = 'adherents';

  protected $hidden = array('password');


  /**
  * Fonction qui récupère les emprunts de l'adhérents
  */
  public function emprunts(){
  	return $this->hasMany('Emprunt','idAdherent');
  }


  // Fontion dernier fonctionnalité
  public function document(){
  	return $this->hasMany('documents','idAdherent');
  }

  
}
 ?>
