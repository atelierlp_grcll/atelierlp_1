﻿<?php


// --- 
// ----------- PAGE D'ACCUEIL 
// --- 
$app->get('/', function () use ($app) {
    $app->redirect($app->urlFor('home'));
});
$app->get('/home', function () use ($app) {

 	$app->render('accueil.twig');
})->name('home');




// --- 
// ---------- Affiche Ajout Type-Genre
// --- 
$app->get('/ajoutTypeGenre', function() use ($app){
	$app->render('ajoutTypeGenre.twig');
});

// ---------- Ajout type -- BDD

$app->post('/ajoutType', function() use ($app){
	$nomType = filter_var($_POST['nomType'], FILTER_SANITIZE_STRING);
	
	$typeExist = Type::where("nomType","LIKE","%$nomType%")->first();

	if( isset($typeExist->id) || empty($nomType) ) {
		$app->redirect($app->urlFor('notife', array(
			'msg' => "Le type $nomType existe déjà  !"
		)));
	} 
	$type = new Type();
	$type->nomType = $nomType;
	$type->save();
	$app->redirect($app->urlFor('notifs', array(
			'msg' => 'Type ajouté'
		)));
});
 
// -------- Ajout Genre -- BDD

$app->post('/ajoutGenre', function() use ($app){

	$nomGenre = filter_var($_POST['nomGenre'], FILTER_SANITIZE_STRING);
	
	$genreExist = Genre::where("nomGenre","LIKE","%$nomGenre%")->first();
	
	if( isset($genreExist->id) || empty($nomGenre) ) {
		$app->redirect($app->urlFor('notife', array(
			'msg' => "Le genre $nomGenre existe déjà  !"
		)));
	} 
	$genre = new Genre();
	$genre->nomGenre = $nomGenre;
	$genre->save();

	$app->redirect($app->urlFor('notifs', array(
		'msg' => "Genre ajouté"
	)));

});



// --- 
// ---------- Affiche Emprunt-Retour
// --- 
$app->get('/empruntRetour', function() use ($app){
	$app->render('empruntRetour.twig');
});


// --------- Emprunt -- BDD

$app->post('/emprunt', function() use ($app){

	$refDoc = filter_var($_POST['refDoc'], FILTER_SANITIZE_STRING);
	$numAdher = filter_var($_POST['numAdher'], FILTER_SANITIZE_NUMBER_INT);

	// Vérification id adherent
	if(empty($numAdher)){
		$app->redirect($app->urlFor('notife', array(
			'msg' => 'Veuillez renseigner le numéro de l\'adhérent'
		)));

	} else {
		// récupération id Adherent
		$adhe = Adherents::find($numAdher);
		if( !isset($adhe->id) ){
			$app->redirect($app->urlFor('notife', array(
				'msg' => "L'adhérent n'existe pas"
			)));
		}
		$adhe->load("emprunts");
		$numAdher = $adhe->id;
	}



	// récupération des documents
	$refs = explode("," , $refDoc);
	//vérifie l'entrée des references
	$docs = array();
	foreach($refs as $ref){
		$test = Documents::whereRaw("reference LIKE '$ref' and (idEtat = 1 or idEtat = 3) ")->first();
		if( !isset( $test->id) ) {
			$app->redirect($app->urlFor('notife', array(
				'msg' => "Le document $ref  n'existe pas ou est emprunté"
			)));
		} else if($test->idEtat === 1 && strtotime(date("d-m-Y")) < strtotime($test->reservation) && $numAdher === $test->idAdherent){
			array_push($docs,$test);
		} else if($test->idEtat === 3 && strtotime(date("d-m-Y")) < strtotime($test->reservation)  && $numAdher !== $test->idAdherent){
			$app->redirect($app->urlFor('notifi', array(
				'msg' => "Le document $ref est réservé ou emprunté par quelqu'un d'autre"
			)));
		} else if($test->idEtat === 3  ){
			$app->redirect($app->urlFor('notifi', array(
				'msg' => "Le document $ref est indisponible"
			)));
		} else {
			array_push($docs,$test);
		}
	}


	// // récupère les documents selon les références ou l'etat est disponible
	// $docs = Documents::whereIn("reference",$refs)->where("idEtat","=",1)->get();
	

	// // Enregistrement dans la base de donnée
	foreach($docs as $document){
		// Enregistrer l'emprunt
		$emprunt = new Emprunt();
		$emprunt->idDoc =  $document->id;
		$emprunt->idAdherent = $numAdher;
		$emprunt->dateDebut = date('d-m-Y');
		$emprunt->dateRetour = date('d-m-Y', strtotime('+30 days'));
		$emprunt->save();

		// MAJ de l'etat
		$document->idEtat = 2;
		$document->reservation = '01-01-1970';
		$document->idAdherent = NULL;
		$document->save();

		
	}

	// Récupere tous les emprunts de l'adherent
	$emprunts = Emprunt::where("idAdherent","=","$numAdher")->with("document")->get();

	// Affichage du titre de chaque emprunt et leur date
	$app->render('emprunt.twig', array(
		'adherent' => $numAdher,
		'emprunts' => $emprunts
	));

});


//-------------- Retour -- BDD

$app->post('/retour', function() use ($app){

	$refDoc = filter_var($_POST['refDoc'], FILTER_SANITIZE_STRING);

	// récupération des documents
	$refs = explode("," , $refDoc);

	// vérifie l'entrée des reference
	foreach($refs as $ref){
		$test = Documents::where("reference","=",$ref)->where("idEtat","=",2)->first();
		if( !isset( $test) ) {
			$app->redirect($app->urlFor('notife', array(
				'msg' => "Le document  $ref  n\'existe pas ou est déjà disponible"
			)));
		}
	}

	// récupère les documents selon les références ou l'etat est disponible
	$docs = Documents::whereIn("reference",$refs)->where("idEtat","=",2)->get();


	// on récupère les id des documents 
	$docTab=array();
	foreach($docs as $doc){
		array_push($docTab,$doc->id);
	}

	// On récupère les retour des adherents + 
	$rendus = Emprunt::whereIn("idDoc",$docTab)->with("Adherents")->with("document")->orderBy('idAdherent', 'ASC')->get();

	// on récupère les idAdherent dans un tableau
	$adhe = array();
	foreach($rendus as $rendu){
		array_push($adhe,$rendu->idAdherent);
	}

	// Enregistrement dans la base de donnée
	foreach($docs as $document){

		// Supprimer les emprunts
		$emprunt = Emprunt::where("idDoc","=",$document->id)->first();

		$emprunt->delete();
		// MAJ de l'etat
		$document->idEtat = 1;
		$document->save();
	}

	// on récupère les emprunts restant des adherents 
	$emprunts = Emprunt::whereIn("idAdherent",$adhe)->with("Adherents")->with("document")->get();

	$app->render('retour.twig', array(
		"docRetour" => $rendus,
		"docEmprunt" => $emprunts
		));

});


// --- 
// ---------- Affiche RetirerEmprunt
// --- 
$app->get('/retirerEmprunt', function() use ($app){
	$app->render('retirerEmprunt.twig');
});


// ---------- RetirerEmprunt -- BDD
$app->post('/retirer', function() use ($app){

	$refDoc = filter_var($_POST['refDoc'], FILTER_SANITIZE_STRING);
	$message = filter_var($_POST['message'], FILTER_SANITIZE_STRING);

	// récupération des documents
	$refs = explode("," , $refDoc);
	// vérifie l'entrée des reference
	foreach($refs as $ref){
		$test = Documents::where("reference","=",$ref)->where("idEtat","=",2)->first();
		if( empty( $test->id) ) {
			$app->redirect($app->urlFor('notife', array(
				'msg' => "Le document n'existe pas ou Le document est déjà disponible ou Le document est indisponible"
			)));
		}
	}	

	$docs = Documents::whereIn("reference",$refs)->where("idEtat","=",2)->get();


	$empruntASupprimer = Emprunt::whereIn("idDoc",$docs)->with("document")->with("Adherents")->get();


	// Enregistrement dans la base de donnée
	$tabRetEmp = array();
	foreach($docs as $document){

		if($document->idEtat === 3 ){
			$app->redirect($app->urlFor('notife', array(
 				'msg' => "Le document $document->reference est indisponible"
 				)));
		} else if($document->idEtat === 1){
			$app->redirect($app->urlFor('notife', array(
 				'msg' => "Le document $document->reference est déjà disponible"
 				)));
		} else {

			// Supprimer les emprunts
			$emprunt = Emprunt::where("idDoc","=",$document->id)->with("document")->with("Adherents")->first();
			array_push($tabRetEmp,$emprunt);
			$emprunt->delete();

			// MAJ de l'etat
			$document->idEtat = 1;
			$document->save();
		}

	}

	$app->render('retirer.twig', array(
		"docRetour" => $tabRetEmp,
		"message" => $message
		));

});

// --- 
// ----------- Affiche GestionDocument 
// --- 
$app->get("/gestionDoc", function() use ($app){
	$documents = Documents::all();
	$types = Type::all();
	$genres = Genre::all();
	$etats = Etat::all();

	$app->render("listingDocs.twig",array(
		'types'  => $types,
    	'genres' => $genres,
    	"etats" => $etats,
		"docs" => $documents
		));
});

// --- 
// ---------- Affiche d'une documents a modification
// --- 
$app->get("/mod-document:id", function($id) use ($app){
	$id = filter_var($id,FILTER_SANITIZE_NUMBER_INT);
	$document = Documents::with('Type')->with("Genre")->with('Etat')->find($id);

	$types = Type::all();
	$genres = Genre::all();
	$etats = Etat::all();

	$app->render("doc.twig",array(
		'types'  => $types,
    	'genres' => $genres,
    	"etats" => $etats,
		"doc" => $document,
		"id" => $id
		));
});


// ---------- Modifier documents 
$app->post("/notifMod", function() use ($app){


	$id = filter_var($_POST["id"],FILTER_SANITIZE_NUMBER_INT);
	$document = Documents::find($id);

	$refDoc = filter_var($_POST["refDoc"],FILTER_SANITIZE_STRING);
	$titre = ($_POST["titreDoc"]);
	$contenu = ($_POST["contenuDoc"]);


	$type = filter_var($_POST["type"],FILTER_SANITIZE_NUMBER_INT);
	$genre = filter_var($_POST["genre"],FILTER_SANITIZE_NUMBER_INT);


	// -------------- IMAGE ---------------
	$contentDirHere = "img/imgDoc/";
	$contentDirBis = "../../ApplicationAdherent/public/img/imgDoc/";

	$img_url = ''; $img_nom = ''; $img_taille = 0; $img_type = '';
	
	if(isset($_FILES['fich'])){
		
		$ret = is_uploaded_file ($_FILES['fich']['tmp_name']);

		if ( $ret ){

			// Le fichier a bien été reçu
			$img_taille = $_FILES['fich']['size'];
			if ( $img_taille > 5097152 ){

		    	 $app->redirect($app->urlFor('notife', array(
		 		  	'msg' => 'Taille de l\'image 1 trop important'
			  		)));
			}

			// Verification de l'extention
			$img_type = $_FILES['fich']['type'];
			if( !strstr($img_type, 'jpg') && !strstr($img_type, 'jpeg') && !strstr($img_type, 'bmp') && !strstr($img_type, 'gif') && !strstr($img_type, 'png')){
				$app->redirect($app->urlFor('notife', array(
		 		  	'msg' => 'Le fichier 1 n\'est pas une image'
			  		)));
			}

			$img_nom = $_FILES['fich']['name'];
			$tmp_file = $_FILES['fich']['tmp_name'];


			// verification de l'upload
			if( !move_uploaded_file($tmp_file, $contentDirHere .$img_nom ) ){

	    		$app->redirect($app->urlFor('notife', array(
		 		  	'msg' => "Impossible de copier le fichier dans app staff"
			  		)));
			}
			if( !copy($contentDirHere.$img_nom, $contentDirBis.$img_nom ) ){

	    		$app->redirect($app->urlFor('notife', array(
		 		  	'msg' => "Impossible de copier le fichier dans app adhérent  "
			  		)));
			}

			$img_url = $contentDirHere . $img_nom;

		}
	} 


	// -------- ajout du document -----------

	$document->reference = $refDoc;
	$document->titre = $titre;
	$document->descriptif = $contenu;
	if(!empty($img_url)){ // si l'image est modifié
	 	// on supprime l'ancienne image
		unlink($document->image);
		unlink("../../ApplicationAdherent/public/".$document->image);
		$document->image = $img_url;
	}

	if(!empty($type) || !empty($genre) || !empty($etat)){
		$document->idType = $type;
		$document->idGenre = $genre;

	} else {
		$app->redirect($app->urlFor('notife', array(
	 		'msg' => "Veuillez compléter les champs déroulants"
 		)));
	}
	$document->save();

	if(isset($document->id)){
		$app->redirect($app->urlFor('notifs', array(
	 		'msg' => "Le document a bien été modifié "
 		)));
	} else{
		$app->redirect($app->urlFor('notifd', array(
	 		'msg' => "Une erreur s'est produit. Contactez l'administrateur"
 		)));
	}


});



// ------------- Ajouter un document
$app->post("/notifAjout",function() use ($app){

	$refDoc = filter_var($_POST["refDoc"],FILTER_SANITIZE_STRING);
	$titre = ($_POST["titreDoc"]);
	$contenu = ($_POST["contenuDoc"]);


	$type = filter_var($_POST["type"],FILTER_SANITIZE_NUMBER_INT);
	$genre = filter_var($_POST["genre"],FILTER_SANITIZE_NUMBER_INT);
	$etat = filter_var($_POST["etat"],FILTER_SANITIZE_NUMBER_INT);


	//  IMAGE 
	$contentDirHere = "img/imgDoc/";
	$contentDirBis = "../../ApplicationAdherent/public/img/imgDoc/";

	$img_url = ''; $img_nom = ''; $img_taille = 0; $img_type = '';
	$ret = is_uploaded_file ($_FILES['fic1']['tmp_name']);

	if ( $ret ){

		// Le fichier a bien été reçu
		$img_taille = $_FILES['fic1']['size'];
		if ( $img_taille > 5097152 ){

	    	 $app->redirect($app->urlFor('notife', array(
	 		  	'msg' => 'Taille de l\'image 1 trop important'
		  		)));
		}

		// Verification de l'extention
		$img_type = $_FILES['fic1']['type'];
		if( !strstr($img_type, 'jpg') && !strstr($img_type, 'jpeg') && !strstr($img_type, 'bmp') && !strstr($img_type, 'gif') && !strstr($img_type, 'png')){
			$app->redirect($app->urlFor('notife', array(
	 		  	'msg' => 'Le fichier 1 n\'est pas une image'
		  		)));
		}

		$img_nom = $_FILES['fic1']['name'];
		$tmp_file = $_FILES['fic1']['tmp_name'];


		// verification de l'upload
		if( !move_uploaded_file($tmp_file, $contentDirHere .$img_nom ) ){

    		$app->redirect($app->urlFor('notife', array(
	 		  	'msg' => "Impossible de copier le fichier dans app staff"
		  		)));
		}
		if( !copy($contentDirHere.$img_nom, $contentDirBis.$img_nom ) ){

    		$app->redirect($app->urlFor('notife', array(
	 		  	'msg' => "Impossible de copier le fichier dans app adhérent  "
		  		)));
		}

		$img_url = $contentDirHere . $img_nom ;

	} else {
		 $app->redirect($app->urlFor('notife', array(
	 		  	'msg' => 'Image obligatoire',
 		  	)));
	}


	// ajout du document 
	$document = new Documents();
	$document->reference = $refDoc;
	$document->titre = $titre;
	$document->descriptif = $contenu;
	$document->image = $img_url;
	$document->reservation = '01-01-1970';

	if(!empty($type) && !empty($genre) && !empty($etat)){
		$document->idType = $type;
		$document->idGenre = $genre;
		$document->idEtat = $etat;

	} else {
		$app->redirect($app->urlFor('notife', array(
	 		'msg' => "Veuillez compléter les champs déroulants"
 		)));
	}


	$document->save();

	if(isset($document->id)){
		$app->redirect($app->urlFor('notifs', array(
	 		'msg' => "Le document s'est bien ajouté"
 		)));
	} else{
		$app->redirect($app->urlFor('notifd', array(
	 		'msg' => "Une erreur s'est produit. Contactez l'administrateur"
 		)));
	}

});

// -------- Suppression du documents
$app->get("/delDoc:id",function($id) use ($app){

	$id = filter_var($id,FILTER_SANITIZE_NUMBER_INT);

	$document = Documents::find($id);
	if(!isset($document->id)){
		$app->redirect($app->urlFor('notifd', array(
	 		'msg' => "Le document n'existe pas"
 		)));
	}

	if(isset($document->id)){

		$urlImage = $document->image;
		unlink($urlImage);
		unlink("../../ApplicationAdherent/public/".$urlImage);
		$document->delete();

		$app->redirect($app->urlFor('notifs', array(
	 		'msg' => "Le documents a bien été supprimé"
 		)));
	} else {
		$app->redirect($app->urlFor('notifd', array(
	 		'msg' => "Une erreur s'est produit. Contactez l'administrateur"
 		)));
	}

});




// --- 
// -------- GESTION ADHÉRENT
// --- 
$app->get('/gestionAdherent', function() use ($app) {

	$adherents = Adherents::all();
	$app->render('gestionAdherent.twig', array(
		'adherents' => $adherents,

		));
});


// -------- AJOUT ADHÉRENT
$app->post('/addAdherent', function() use ($app) {

	$adherent = new Adherents();
	$adherent->prenom = filter_var($app->request->post('prenomA'),FILTER_SANITIZE_STRING);
	$adherent->nom = filter_var($app->request->post('nomA'),FILTER_SANITIZE_STRING);
	$adherent->email = filter_var($app->request->post('mailA'),FILTER_SANITIZE_EMAIL);
	$adherent->dateAdhesion = date('d-m-Y');

	// mot de passe 
	$adherent->password = crypt($app->request->post('passwordA'), '$5$rounds=5000$utiliseselpourmedianet$');
	$adherent->save();
	$app->render('adherentCreer.twig', array(
		'adherent' => $adherent,
		'mdp' => $app->request->post('passwordA')
	));
});


// -------- MODIFIÉ ADHÉRENT
$app->post('/modAdherent', function() use ($app) {
	
	$id = filter_var($app->request->post('idAdherentM'),FILTER_SANITIZE_NUMBER_INT);
	$prenom = filter_var($app->request->post('prenomM'),FILTER_SANITIZE_STRING);
	$nom = filter_var($app->request->post('nomM'),FILTER_SANITIZE_STRING);
	$email = filter_var($app->request->post('mailM'),FILTER_SANITIZE_EMAIL);
	$password = $app->request->post('passwordM');
	
	if(!empty($id)){
		$adherent = Adherents::find($id);
		
	    if(isset($adherent->id)){
	      
			if(!empty($nom))
				$adherent->nom =$nom;
			
			if(!empty($prenom))
				$adherent->prenom = $prenom;
			
			if(!empty($email))
				$adherent->email = $email;
			
			if(!empty($password))
				$adherent->password =  crypt($password, '$5$rounds=5000$utiliseselpourmedianet$');
			
			$adherent->save();

			$app->redirect($app->urlFor('notifs', array(
				'msg' => "Adhérent modifié"
			)));

		} else {

			$app->redirect($app->urlFor('notife', array(
		  		'msg' => "Adhérent inexistant"
			)));
		}

	} else {
		$app->redirect($app->urlFor('notife', array(
			'msg' => "Renseigner un ID Adhérent correct"
		)));
	}

});


$app->post('/delAdherent', function() use ($app) {

	$id = filter_var($app->request->post('idAdherentS'),FILTER_SANITIZE_NUMBER_INT);
	
	if(!empty($id)){

		$adherent = Adherents::find($id);

		if(isset($adherent->id)){
      		$adherent->delete();
      	} else {
      		$app->redirect($app->urlFor('notife', array(
          		'msg' => "Adhérent inexistant"
      		)));
      	}
		$app->redirect($app->urlFor('notifs', array(
   		 	'msg' => "Adhérent supprimé"
 		)));
	}
	
});







// --- 
// ---------------------- PAGES DE NOTIFICATIONS
// --- 
$app->get('/danger:msg', function ($msg="") use ($app) {

	$app->render('notification.twig', array(
		'msg' => $msg,
		'coulAlert' => 'alertDanger',
		'titreAlert' => 'Attention !'
	));
})->name('notifd');

$app->get('/success:msg', function ($msg ='') use ($app) {

	$app->render('notification.twig', array(
		'msg' => $msg,
		'coulAlert' =>'alertSuccess',
		'titreAlert' => 'Succès'
	));
})->name('notifs');


$app->get('/info:msg', function ($msg="") use ($app) {

	$app->render('notification.twig', array(
		'msg' => $msg,
		'coulAlert' => 'alertInfo',
		'titreAlert' =>'Information'
	));
})->name('notifi');

$app->get('/erreur_:msg', function ($msg="") use ($app) {

	$app->render('notification.twig', array(
		'msg' => $msg,
		'coulAlert' =>'alertAdvert',
		'titreAlert' =>'Erreur'
	));
})->name('notife');
 ?>
