-- BASE DE DONNEE ATELIER 1 


SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


--
-- Base de données :  `medianets`
--
-- --------------------------------------------------------

CREATE DATABASE IF NOT EXISTS `medianet` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `medianet`;

--
-- Structure de la table `Emprunt`
--
DROP TABLE IF EXISTS `emprunt`;
CREATE TABLE IF NOT EXISTS `emprunt`(
	`id` int(11) NOT NULL,
	`idDoc` int(11) NOT NULL,
	`idAdherent` int(11) NOT NULL,
	`dateDebut` varchar(200),
	`dateRetour` varchar(200),

	`updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  	`created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=0;

 INSERT INTO `emprunt` (`id`, `idDoc`, `idAdherent`, `dateDebut`,`dateRetour`,`updated_at`, `created_at`) VALUES
 (1,1,1,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59'),
 (2,2,1,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59'),
 (5,3,2,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59'),
 (9,4,3,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59'),
 (15,5,4,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59'),
 (13,8,4,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59'),
 (19,9,5,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59'),
 (3,11,1,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59'),
 (6,12,2,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59'),
 (7,13,2,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59'),
 (10,14,3,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59'),
 (14,17,4,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59'),
 (16,19,5,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59'),
 (4,21,1,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59'),
 (8,22,2,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59'),
 (11,23,3,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59'),
 (12,24,3,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59'),
 (17,26,5,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59'),
 (18,28,5,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59'),
 (20,30,5,'05-11-2015','05-12-2015','2015-10-20 09:00:56', '2015-10-18 21:58:59');

--
-- Structure de la table `Documents`
--

DROP TABLE IF EXISTS `documents`;
CREATE TABLE IF NOT EXISTS `documents`(
	`id` int(11) NOT NULL,
	`reference` varchar(50) NOT NULL ,
	`titre` varchar(50),
	`descriptif` varchar(10000),
	`image` varchar(250) NOT NULL,
	`reservation` varchar(250) DEFAULT '01-01-1970',

	`idEtat` int(11) NOT NULL,
	`idType` int(11) NOT NULL,
	`idGenre` int(11) NOT NULL,
	`idAdherent` int(11),

	`updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  	`created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
 ) ENGINE=InnoDB AUTO_INCREMENT=4;



--
-- Contenu de la table `Documents`
--

INSERT INTO `documents` (`id`, `Reference`, `Titre`, `Descriptif`,`image`,`idEtat`,`idType`,`idGenre`) VALUES
(1, 'REF205E', 'Martine à la mer', 'C\'est martine qui va à la mer','img/imgDoc/martine_mer.jpg' ,2,2,6),
(2, 'REF206E', 'Le livre de la jungle', "Avant d'aller se coucher quoi de mieux qu'un merveilleux conte Disney ? Après la lecture d'un texte court et simple, les tout-petits s'endormiront avec de jolies images plein les yeux.",'img/imgDoc/livre_de_la_jungle.jpg' ,2,2,6),

(3, 'REF207E', 'Ceux qui tombent', "Retraite ou pas retraite ? L’inspecteur Harry Bosch s’interroge encore quand on le charge de deux dossiers… Le premier – le viol suivi du meurtre d’une jeune fille – remonte à 1989 et les tests ADN viennent de désigner le coupable : Clayton Pell… 8 ans au moment des faits ! Peu crédible, donc. Dans le même temps, Bosch est appelé sur une scène de crime au Château Marmont, le célèbre hôtel de Los Angeles, où un homme est tombé du septième étage. Suicide, crime ou accident ? Le père de la victime, un conseiller municipal très puissant à L.A., veut en avoir le coeur net et exige assez curieusement que l’enquête soit confiée à Bosch, son vieil ennemi de toujours. Politiciens corrompus, prédateurs sexuels… Harry Bosch a fort à faire, et surtout pas le temps de s’apitoyer sur lui-même.
Un double sac de noeuds que Connelly dénoue avec un indéfectible talent, tout en finissant, c’est sa touche, par un revers rageur. Julie Malaure, Le Point.",'img/imgDoc/ceux_qui_tombent.jpg',1,2,1),
(4, 'REF208E', 'Une main encombrante', "C'est l'automne en Scanie avec son lot de pluie et de vent. Désabusé, Wallander aspire à une retraite paisible et rêve d'avoir une maison à la campagne et un chien. Il visite une ancienne ferme, s'enthousiasme pour les lieux, pense avoir trouvé son bonheur. Pourtant, lors d'une dernière déambulation dans le jardin à l'abandon, il trébuche sur ce qu'il croit être les débris d'un râteau. Ce sont en fait les os d'une main affleurant le sol. Les recherches aboutissent à une découverte encore plus macabre. En lieu et place d'une maison, Wallander récolte une enquête. ",'img/imgDoc/une_main_encombrante.jpg' ,1,2,1),

(5, 'REF209E', 'Treize raisons', "Clay Jensen reçoit sept cassettes enregistrées par Hannah Baker avant qu'elle ne se suicide. Elle y parle de treize personnes qui ont, de près ou de loin, influé sur son geste. Et Clay en fait partie. D'abord effrayé, Clay écoute la jeune fille en se promenant au son de sa voix dans la ville endormie. Puis, il découvre une Hannah inattendue qui lui dit à l'oreille que la vie est dans les détails. Une phrase, un sourire, une méchanceté ou un baiser et tout peut basculer.",'img/imgDoc/treize_raisons.jpg' ,2,2,2),
(6, 'REF210E', 'Ils ont laissé papa revenir', "Elle pensait être protégée des agressions physiques et psychologiques subies depuis sa petite enfance… Toni Maguire, auteur du best-seller Ne le dis pas à maman, poursuit le récit de son enfance, racontant sa terrible vérité. Son père a abusé d’elle dès l’âge de 6 ans et ce crime n’a été révélé que lorsqu’elle est tombée enceinte et a dû se faire avorter, à 14 ans. C’est grâce au témoignage difficile de Toni que cet homme qui lui a fait tant de mal a été emprisonné. Enfin, elle a cru pouvoir vivre normalement. Jusqu’au jour où son père, sorti de prison, est revenu à la maison.",'img/imgDoc/ils_ont_laisse_papa_revenir.jpg' ,1,2,2),

(7, 'REF211E', 'Petit livre de - 150 idées pour emmerder le monde', "Un nouveau titre de Laurent Gaulet dans la collection Petit livre… spécial mauvais esprit ! 150 idées pour faire tourner en bourrique anonymes ou amis. Glisser un string dans le caddie d’une petite mamie juste avant qu’elle ne passe à la caisse, se garer en biais sur trois places sur le parking d’Auchan un samedi après-midi, tenir une conversation très intime dans le métro à l’heure de pointe… Un Petit livre essentiel pour sourire à ces idées sournoises ou s’en inspirer en cas de coup dur !",'img/imgDoc/emmerder_le_monde.jpg' ,1,2,3),
(8, 'REF212E', 'Le petit livre des anecdotes les plus drôles', "Condensé d'anecdotes droles",'img/imgDoc/anecdotes_drole.jpg' ,2,2,3),

(9, 'REF213E', 'Le Silmarillion', "Les Premiers Jours du Monde étaient à peine passés quand Fëanor, le plus doué des Elfes, créa les trois Silmarils. Ces bijoux renfermaient la Lumière des Deux Arbres de Valinor. Morgoth, le premier Prince de la Nuit, était encore sur la Terre du Milieu, et il fut fâché d'apprendre que la Lumière allait se perpétuer. Alors il enleva les Silmarils, les fit sertir dans son diadème et garder dans la forteresse d'Angband. Les Elfes prirent les armes pour reprendre les joyaux et ce fut la première de toutes les guerres. Longtemps, longtemps après, lors de la Guerre de l'Anneau, Elrond et Galadriel en parlaient encore. L'œuvre complète de J.R.R. Tolkien (1892-1973) figure au catalogue de Pocket.",'img/imgDoc/silmarillion.jpg' ,2,2,4),
(10, 'REF214E', 'La stratégie Ender', "La Stratégie Ender est un roman de science-fiction de l'écrivain américain Orson Scott Card, publié en 1985, qui reprend et développe une nouvelle du même auteur publiée en 1977 dans le magazine Analog. C'est son roman le plus célèbre.",'img/imgDoc/strtegie_ender.jpg' ,1,2,4),

(11, 'REF215E','Live 2015 patrick bruel','Concert de patrick CD+DVD','img/imgDoc/patrick_bruel_concert.jpg',2,1,8),
(12, 'REF216E','Les Légendes De La Chanson Française - Les Indispensables','Les indispensables de la chanson francaise','img/imgDoc/les_indispensables.jpg',2,1,8),

(13, 'REF217E','Let There Be Rock','Let there be rock ACDC','img/imgDoc/let_there_be_rock.jpg',2,1,9),
(14, 'REF218E','Deep Purple in Rock','Deep Purple in Rock','img/imgDoc/deep_purple_in_rock.jpg',2,1,9),

(15, 'REF219E','Rendez-Vous au Bal Musette, Vol. 1','Rendez-Vous au Bal Musette, Vol. 1','img/imgDoc/rdv_musette.jpg',1,1,10),
(16, 'REF220E','Anthologie du musette','Anthologie du musette','img/imgDoc/anthologie_musette.jpg',1,1,10),

(17, 'REF221E','Les Plus Grands Tubes Disco','Les Plus Grands Tubes Disco','img/imgDoc/grand_disco.jpg',2,1,11),
(18, 'REF222E','Disco Nostalgie /Vol.2','Disco Nostalgie /Vol.2','img/imgDoc/disco_nostalgie.jpg',1,1,11),

(19, 'REF223E','Thirty-One','Thirty-One Jana Kramer','img/imgDoc/thirty_one.jpg',2,1,12),
(20, 'REF224E','Mockingbird','Mockingbird','img/imgDoc/mockingbird.jpg',1,1,12),

(21, 'REF225E', 'L\'age de glace','sid, many et diego partent à l\'aventure','img/imgDoc/age_de_glace.jpg' ,2,3,13),
(22, 'REF226E', 'La Reine Des Neiges',"une reine et de la neige...",'img/imgDoc/reine_neige.jpg' ,2,3,13),

(23, 'REF227E', 'Taken 3','Touche pas a sa fille','img/imgDoc/taken_3.jpg' ,2,3,14),
(24, 'REF228E', 'Fast & Furious 7',"des voitures qui roulent vites",'img/imgDoc/fast_furious.jpg' ,2,3,14),

(25, 'REF229E', 'Renoir',"1915. Au crépuscule de sa vie, Auguste Renoir est éprouvé par la perte de son épouse, les douleurs du grand âge, et les mauvaises nouvelles venues du front : son fils Jean est blessé... Mais une jeune fille, Andrée, apparue dans sa vie comme un miracle, va insuffler au vieil homme une énergie qu il n attendait plus. Éclatante de vitalité, rayonnante de beauté, Andrée sera le dernier modèle du peintre, sa source de jouvence. Lorsque Jean, revenu blessé de la guerre, vient passer sa convalescence dans la maison familiale, il découvre à son tour, fasciné, celle qui est devenue l astre roux de la galaxie Renoir. ",'img/imgDoc/renoir.jpg' ,1,3,15),
(26, 'REF230E', 'Grace de Monaco',"Lorsqu'elle épouse le Prince Rainier en 1956, Grace Kelly est alors une immense star de cinéma, promise à une carrière extraordinaire. Six ans plus tard, alors que son couple rencontre de sérieuses difficultés, Alfred Hitchcock lui propose de revenir à Hollywood, pour incarner Marnie dans son prochain film. Mais c'est aussi le moment ou la France menace d'annexer Monaco, ce petit pays dont elle est maintenant la Princesse. Grace est déchirée. Il lui faudra choisir entre la flamme artistique qui la consume encore ou devenir définitivement : Son Altesse Sérénissime, la Princesse Grace de Monaco. ", 'img/imgDoc/grace.jpg' ,2,3,15),

(27, 'REF231E', 'Les Chevaliers du Fiel - Le best ouf',"Plus qu'une simple compilation de leurs meilleurs sketches, il s'agit d'un spectacle où le duo interprète, sur scène, tous leurs sketches cultes et des inédits...",'img/imgDoc/fizl.jpg' ,1,3,3),
(28, 'REF232E', 'Les cléfs de bagnole',"Les clefs de bagnole est l'histoire d'un type qui cherche... ses clefs de bagnole. Il va lui falloir un peu plus d'une heure et demi pour s'apercevoir qu'elles étaient dans sa poche... ",'img/imgDoc/cle_bagnole.jpg' ,2,3,3),

(29, 'REF233E', 'Borderline',"Au petit matin, l'IGS débarque dans la maison du commissaire Blain. Il est placé en garde-à-vue pour association de malfaiteurs, trafic de stupéfiants, vol en réunion et détournement de scellés. Après 25 ans d'une carrière irréprochable, Blain va être confronté, de l'autre côté du miroir, à sa propre réalité professionnelle... ",'img/imgDoc/borderline.jpg' ,1,3,1),
(30, 'REF234E', 'L\'Affaire SK1',"Paris début des années 90 un jeune inspecteur fait ses premiers pas à la Police Judiciaire au 36 quai des Orfèvres. Sa première enquête porte sur l'assassinat d'une jeune fille. Son travail le conduit à étudier des dossiers similaires qu'il est le seul à connecter ensemble. Pendant 7 ans, obsédé par ces meurtres sauvages qu'il rapproche il ne va cesser de traquer le meurtrier. Ce policier devient l'architecte de l'affaire la plus complexe qu'ait eu à traiter la police judiciaire de l'époque avec un fichier ADN qui n'existait pas encore... Un film palpitant sur la traque de Guy Georges, le tueur de l'est Parisien. ",'img/imgDoc/sk1.jpg' ,2,3,1);


--
-- Structure de la table `Type`
--
DROP TABLE IF EXISTS `type`;
CREATE TABLE IF NOT EXISTS `type`(
	`id` int(11) NOT NULL,
	`nomType` varchar(50),
	 `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4;

--
-- Contenu de la table `Type`
--

INSERT INTO `type` (`id`, `nomType`) VALUES
(1, 'CD'),
(2, 'Livre'),
(3, 'DVD');


--
-- Structure de la table `Genre`
--
DROP TABLE IF EXISTS `genre`;
CREATE TABLE IF NOT EXISTS `genre`(
	`id` int(11) NOT NULL,
	`nomGenre` varchar(50),
	 `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  	`created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=10;


--
-- Contenu de la table `Genre`
--

INSERT INTO `genre` (`id`, `nomGenre`) VALUES
(1, 'Policier'),
(2, 'Drame'),
(3, 'Comique'),
(4, 'Science fiction'),
(5, 'Educatif'),
(6, 'Enfants'),
(7, 'Bande dessiné'),
(8, 'Variété francaise'),
(9, 'Rock'),
(10, 'Musette'),
(11, 'Disco'),
(12, 'Country'),
(13, 'Dessin animé'),
(14, 'Action'),
(15, 'Biographie');




--
-- Structure de la table `etat`
--

DROP TABLE IF EXISTS `etat`;
CREATE TABLE IF NOT EXISTS `etat`(
	`id` int(11) NOT NULL,
	`nomEtat` varchar(50),
	 `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4;

--
-- Contenu de la table `Etat`
--

INSERT INTO `etat` (`id`, `nomEtat`) VALUES
(1, 'Disponible'),
(2, 'Emprunté'),
(3, 'Indisponible');





--
-- Structure de la table `Adherents`
--
DROP TABLE IF EXISTS `adherents`;
CREATE TABLE IF NOT EXISTS `adherents`(
	`id` int(11) NOT NULL,
	`prenom` varchar(50),
	`nom` varchar(250),
	`email` varchar(250),
	`password` varchar(250),
	`dateAdhesion` varchar(11),
	 `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE=InnoDB AUTO_INCREMENT=5;

INSERT INTO `adherents` (`id`,`prenom`,`nom`,`email`,`password`,`dateAdhesion`) VALUES
(1,'Sylvie','Robert','sylvie@robert.com','$5$rounds=5000$utiliseselpourme$Bq6BmUYMrBIfsmHqUNAAiM2rGInH1bB23WXodzpuTPD','06-11-2015'),

(2,'Paul','mccartney','Paul.mccartney@gmail.com','$5$rounds=5000$utiliseselpourme$Bq6BmUYMrBIfsmHqUNAAiM2rGInH1bB23WXodzpuTPD','06-11-2015'),

(3,'Jean','Jean','Jeannot@hotmail.fr','$5$rounds=5000$utiliseselpourme$Bq6BmUYMrBIfsmHqUNAAiM2rGInH1bB23WXodzpuTPD','06-11-2015'),

(4,'Sarah','Croche','SarahCroche@orange..fr','$5$rounds=5000$utiliseselpourme$Bq6BmUYMrBIfsmHqUNAAiM2rGInH1bB23WXodzpuTPD','06-11-2015'),

(5, 'Johny','Depp','Jacksparrox@pirate.com','$5$rounds=5000$utiliseselpourme$Bq6BmUYMrBIfsmHqUNAAiM2rGInH1bB23WXodzpuTPD','06-11-2015');



--
-- PRIMARY KEY
--

ALTER TABLE `etat`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `genre`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `adherents`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `emprunt`
  ADD PRIMARY KEY (`id`);


--
-- AUTO INCREMENT 
--

ALTER TABLE `etat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;

ALTER TABLE `genre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;

ALTER TABLE `type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;

ALTER TABLE `documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;

ALTER TABLE `adherents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=0;

ALTER TABLE `emprunt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=0;
--
-- FOREIGN KEY
--

ALTER TABLE `emprunt`
     ADD CONSTRAINT `fk_emp_doc`  FOREIGN KEY (`idDoc`) REFERENCES documents(`id`),
     ADD CONSTRAINT `fk_emp_adh` FOREIGN KEY (`idAdherent`) REFERENCES adherents(`id`);


ALTER TABLE `documents`
  ADD CONSTRAINT `fk_doc_etat` FOREIGN KEY (`idEtat`) REFERENCES etat(`id`),
  ADD CONSTRAINT `fk_doc_type` FOREIGN KEY (`idType`) REFERENCES type(`id`),
  ADD CONSTRAINT `fk_doc_genre` FOREIGN KEY (`idGenre`) REFERENCES genre(`id`),
  ADD CONSTRAINT `fk_doc_adh` FOREIGN KEY (`idAdherent`) REFERENCES adherents(`id`);;

